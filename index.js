// Описати своїми словами навіщо потрібні функції у програмуванні.
//Они нужны для исключения повторения одинакового кода.

// Описати своїми словами, навіщо у функцію передавати аргумент.
//Аргументы функции подразумевают под собой локальные переменные,
//которые не изменяют глобальные перменные и работают исключительно в теле функции.

// Що таке оператор return та як він працює всередині функції?
//Return останавливает выполнение функции в том месте, где он находится.
//Return может также вернуть значение неких вычислений/сравнений в код, которые ее вызвал.

//Короткий вариант

let numberOne;
let mathOperation;
let numberTwo;

while (
  !numberOne ||
  !numberTwo ||
  !isFinite(numberOne) ||
  !isFinite(numberTwo)
) {
  numberOne = prompt("Enter 1st number", numberOne);
  mathOperation = prompt("Enter math operation", mathOperation).trim();
  numberTwo = prompt("Enter 2nd nummber", numberTwo);
}

console.log("First number :>> ", numberOne);
console.log("Math operation :>> ", mathOperation);
console.log("Second number :>> ", numberTwo);
console.log("Result :>> ", doSomeMath(numberOne, numberTwo));
alert(doSomeMath(numberOne, numberTwo));

function doSomeMath(a, b) {
  switch (mathOperation) {
    case "+":
      return +a + +b;
      break;
    case "-":
      return a - b;
      break;
    case "*":
      return a * b;
      break;
    case "/":
      return a / b;
    default:
  }
}

console.log("===========================");

// Длинный вариант

// const numberOne = +prompt("Enter first number", numberOne);
// const mathOperation = prompt("Enter math operation", mathOperation);
// const numberTwo = +prompt("Enter second number", numberTwo);

// console.log("numberOne :>> ", numberOne);
// console.log("mathOperation :>> ", mathOperation);
// console.log("numberTwo :>> ", numberTwo);

// function plus(a = numberOne, b = numberTwo) {
//   return a + b;
// }

// function minus(a = numberOne, b = numberTwo) {
//   return a - b;
// }

// function divide(a = numberOne, b = numberTwo) {
//   return a / b;
// }

// function multiply(a = numberOne, b = numberTwo) {
//   return a * b;
// }

// if (mathOperation === "+") {
//   console.log(plus());
// } else if (mathOperation === "-") {
//   console.log(minus());
// } else if (mathOperation === "/") {
//   console.log(divide());
// } else if (mathOperation === "*") {
//   console.log(multiply());
// }
